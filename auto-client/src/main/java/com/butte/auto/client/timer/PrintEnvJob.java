package com.butte.auto.client.timer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Map;

// @Component
public class PrintEnvJob {

    private static final Logger LOG = LoggerFactory.getLogger(PrintEnvJob.class.getName()) ;
    /**
     * 启动类注解：@EnableScheduling
     * 每60秒执行一次
     */
    // @Scheduled(fixedDelay = 60000)
    public void systemData () {
        Map<String,String> envMap = System.getenv();
        for (Map.Entry<String, String> entry:envMap.entrySet()){
            String key = entry.getKey();
            String value = entry.getValue();
            LOG.info("【key:{},value:{}】",key,value);
        }
    }
}
