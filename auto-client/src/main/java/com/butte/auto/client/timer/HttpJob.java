package com.butte.auto.client.timer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

//@Component
public class HttpJob {

    private static final Logger LOG = LoggerFactory.getLogger(HttpJob.class.getName()) ;

    private static final String SERVER_URL = "http://localhost:8082/serve";

    /**
     * 启动类注解：@EnableScheduling
     * 每10秒执行一次
     */
    //@Scheduled(fixedDelay = 10000)
    public void systemDate (){
        try{
            SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
            factory.setReadTimeout(3000);
            factory.setConnectTimeout(6000);
            RestTemplate restTemplate = new RestTemplate(factory);
            Map<String,String> paramMap = new HashMap<>() ;
            String result = restTemplate.getForObject(SERVER_URL,String.class,paramMap);
            LOG.info("server-resp::::"+result);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}

